/**********************************************************************
 * bc28.h
 *Copyright:(C)ZhouYanlin<www.zhouyanlin1222@qq.com>
 *
 *Description: for NB-IoT to community
 *
 *
 *  Created on: 2021年6月3日
 *      Author: ZhouYanlin
 **********************************************************************/

#ifndef INC_BC28_H_
#define INC_BC28_H_

#include "at_cmd.h"

/* NB-IoT 模块复位重启函数。返回值为0 表示成功，!0表示失败 */
extern int nb_iot_reset(void);


/* NB-IoT 初始化模块函数。
 * 函数说明：
 * 传入一个需要设定的频段，发送AT测试模块是否正常工作
 * 然后重启模块，将模块设定为此频段，最后检测读卡是否成功
 *
 * 输入参数：
 * @ frequency_band_at：需要设定的工作频段
 *
 * 返回值：0 表示成功； -1传入参数有误;  -2表示模块工作异常;
 * 		  -3表示重启模块失败；-5表示设置频段失败；
 * 		  -7表示读取SIM卡失败
 *
 *  */
extern int nb_iot_module_init(char *frequency_band_at);


/* NB-IoT 模块连接云平台函数
 * 函数说明：
 * 传入输入连接云平台的IP和port的AT指令，连接相应的云平台
 * 并发起云平台注册指令，最后返回注册的结果
 *
 * 输入参数：
 * @frequency_band_at:云平台的IP+port的AT指令字符串
 *
 * 返回值：0 表示成功；-1表示输入参数非法；
 * 		   -2表示发送注册云平台命令失败；
 * 		   -3表示注册云平台失败
 *
 *  */
extern int nb_iot_connect(char *paltform_at);


/* NB-IoT 模块获取信号强度。返回值为>= 0 表示信号强度，< 0 表示失败 */
extern int nb_iot_get_sign(void);


/* NB-IoT 模块发送光照强度函数
 * 函数说明：
 * 传入整型的光照强度，将它发送给连接的云平台
 *
 * 传入参数;
 * @Lux:整型的光照强度
 *
 * 返回值:0 表示发送成功，-1表示发送失败
 *
 */
extern int nb_iot_send_lux(int Lux);


/* NB-IoT 模块发送温湿度函数
 * 函数说明：
 * 传入需要发送的符点数温湿度值，将他们发送到连接的云平台上
 *
 * 传入参数：
 * @temperature：浮点型温度值
 * @humidity：浮点型湿度值
 *
 * 返回值：0 表示发送成功；-2表示发送温度失败；
 * 			-3表示发送湿度失败
 *
 */
extern int nb_iot_send_TH(float temperature, float humidity);


/* NB-IoT 模块发送信号强度函数。返回值为0 表示失败，!0表示发送的字节数  */
extern int nb_iot_send_sign(int sign);


/* 解析云平台发送来的指令，根据指令操作继电器函数
 * 函数说明;
 * 接收并解析云平台发送来的指令，根据指令操作继电器和控制LED灯的亮灭
 *
 * 返回值为0 表示执行成功，-1表示命令有误；
 *
 * */
extern int parse_recv_cmd(char * cmd);


#endif /* INC_BC28_H_ */
