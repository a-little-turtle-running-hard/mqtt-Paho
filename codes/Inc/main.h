/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define AdcLux_Pin GPIO_PIN_4
#define AdcLux_GPIO_Port GPIOA
#define Relay1_Pin GPIO_PIN_1
#define Relay1_GPIO_Port GPIOB
#define Relay2_Pin GPIO_PIN_3
#define Relay2_GPIO_Port GPIOB
#define SysLed_Pin GPIO_PIN_4
#define SysLed_GPIO_Port GPIOB
#define BlueLed_Pin GPIO_PIN_5
#define BlueLed_GPIO_Port GPIOB
#define RedLed_Pin GPIO_PIN_6
#define RedLed_GPIO_Port GPIOB
#define GreenLed_Pin GPIO_PIN_7
#define GreenLed_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

#define	time_after(a,b) 		( (int32_t)(b) - (int32_t)(a) < 0 )

#define time_before(a,b) 		time_after(b,a)

#define	time_after_eq(a,b) 		( (int32_t)(a) - (int32_t)(b) >= 0 )

#define time_before_eq(a,b) 	time_after_eq(b,a)

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
