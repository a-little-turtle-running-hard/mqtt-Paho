/**********************************************************************
 * sht30.c
 *Copyright:(C)ZhouYanlin<www.zhouyanlin1222@qq.com>
 *
 *Description:  Common AT command source code
 *
 *
 *  Created on: 2021年5月30日
 *      Author: ZhouYanlin
 **********************************************************************/

#ifndef INC_AT_CMD_H_
#define INC_AT_CMD_H_

#include "usart.h"

/* 以下变量、函数及宏定义以在上层USART定义 实现代码分层
 * 需要使用本文件的函数实现，需要在USART层实现如下功能函数作为前提
 * USART初始化、USART发送数据、USART接收数据
 * */
#if 0
#define	at_uart			  &huart3		     /* at_cmd 使用的串口 */
extern char               g_atcmd_rxbuf[256];/* at指令模块的接收buffer */
extern uint8_t            g_atcmd_bytes;	 /* at指令模块接收的数据大小 */
#define clear_uart3_rxbuf() do { memset(g_atcmd_rxbuf, 0, sizeof(g_atcmd_rxbuf)); g_atcmd_bytes=0; } while(0) /*清除接收buffer的宏 */

/* USART3 init function */
void MX_USART3_UART_Init(void)
{
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 9600;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }

  /* USART receive msg function */
  HAL_UART_Receive_IT(&huart3 , &s_atcmd_rxch, 1);
}

/* USART send msg function */
HAL_StatusTypeDef HAL_UART_Transmit(UART_HandleTypeDef *huart, const uint8_t *pData, uint16_t Size, uint32_t Timeout);

#endif


/* 函数说明：给模块发送一个AT命令，并在超时时间内判断是否收到所期待的字符串。如果传入了一个buf，则将收到的AT命令回复数据通过 outbuf 返回出去。
 * 输入参数：
 * $atcmd: 要发送的 AT 命令
 * $expect_reply: 发送AT命令期待的回复，如果不指定则默认等待 "OK\r\n"传入NULL即可;
 * $timeout: 等待接收AT命令回复数据的超时时间
 *
 * 输出参数：
 * $outbuf: 接收AT命令回复数据的buf，如果传 NULL 则不会返回给调用函数；
 * $size_outbuf: 接收buf的大小
 *
 *   返回值：    -1: 输入参数出错；   0: AT命令收到期待的字符串   -2: 接收AT命令回复超时    -3: 收到 ERROR 等字符串
 *
 */
extern int send_atcmd(char *atcmd, char *expect_reply, unsigned int timeout, char *outbuf, int size_outbuf);


/* Description: Send AT command which will only reply by "OK" or "ERROR", such as AT+RST:
 * Reply:   \r\nREBOOT_CAUSE_SECURITY_PMU_POWER_ON_RESET\r\nNeul\r\nOK\r\n
 * Return Value: 0: OK     -X: Failure
 *
 */
extern int send_atcmd_check_str(char *atcmd, char *expect_reply, unsigned int timeout);


/* Description: Send AT command which will only reply by "OK" or "ERROR", such as AT:
 * Reply:   \r\nOK\r\n
 * Return Value: 0: OK     -X: Failure
 *
 */
extern int send_atcmd_check_ok(char *atcmd, unsigned int timeout);

/*
 *  Description: Send AT command which will reply by a value directly in a single line, such as AT+CGMM:
 *                  Reply:   \r\nEC20F\r\nOK\r\n
 *
 * Return Value: 0: OK     -X: Failure
 */
extern int send_atcmd_check_value(char *atcmd, unsigned int timeout, char *reply, int size_reply);


/*
 *  Description: Send AT command which will reply by the value  with a prefix "+CMD: " line, such as AT+CSQ:
 *                  Reply:   \r\n+CSQ: 26,99\r\nOK\r\n
 *
 * Return Value: 0: OK     -X: Failure
 */
extern int send_atcmd_check_request(char *atcmd, unsigned int timeout, char *reply, int size_reply);


/* 函数说明： 给模块发送"AT"命令，确认模块是否正常回复。
 * 输入参数： $times: 尝试发送 AT 命令测试的次数
 *  返回值： 0: 发送AT命令收到OK回复     <0: 发送AT命令等待回复失败。
 */
extern int send_atcmd_check_module(void);


/* 函数说明： 给模块发送"ATE0"关闭回显，或 "ATE1" 打开回显。
 * 输入参数： $on: 打开还是关闭回显,ECHO_OFF 或  ECHO_ON
 *  返回值： 0: 发送成功     <0: 发送失败。
 */
enum
{
	ECHO_OFF,
	ECHO_ON,
};
extern int set_atcmd_echo(int status);


/* NB-IoT 模块发送AT命令函数。返回值为0 表示成功，!0表示失败 */
//extern int send_atcmd(char *atcmd, char *expect_reply, unsigned int timeout);

#endif /* INC_AT_CMD_H_ */
