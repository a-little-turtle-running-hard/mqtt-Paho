/**********************************************************************
 * sht30.h
 *Copyright:(C)ZhouYanlin<www.zhouyanlin1222@qq.com>
 *
 *Description: for sht30 lowlevel API with IIC
 *
 *
 *  Created on: 2021年7月3日
 *      Author: ZhouYanlin
 **********************************************************************/
#ifndef INC_SHT30_H_
#define INC_SHT30_H_

#include "stm32l4xx_hal.h"

/* chip 7-bits hardware address */
#define SHT30_ADDR         0x44    /* ADDR connected to GND */

/* I2C protocol communication address */
#define SHT30_ADDR_WR      (SHT30_ADDR<<1)            /* address bit[0]=0 is write */
#define SHT30_ADDR_RD      ((SHT30_ADDR<<1) | 0x01)   /* address bit[0]=1 is read  */

#define SHT30_DATA_SIZE    6  /* 2B temperature + 1B CRC, 2B humidity + 1B CRC  */

typedef enum
{
    /* Soft reset command  */
    SOFT_RESET_CMD = 0x30A2,

    /* Measurement Commands for Single Shot Data Acquisition Mode:
     * Repeatability (low,medium and high) and clock stretching (enabled ordisabled)
     */
    HIGH_ENABLED_CMD    = 0x2C06,
    MEDIUM_ENABLED_CMD  = 0x2C0D,
    LOW_ENABLED_CMD     = 0x2C10,
    HIGH_DISABLED_CMD   = 0x2400,
    MEDIUM_DISABLED_CMD = 0x240B,
    LOW_DISABLED_CMD    = 0x2416,

    /*  Measurement Commands for Periodic Data Acquisition Mode
     *  Data acquisition frequency: 0.5, 1, 2, 4 & 10 measurements per second, mps
     */
    HIGH_0_5_CMD   = 0x2032,
    MEDIUM_0_5_CMD = 0x2024,
    LOW_0_5_CMD    = 0x202F,
    HIGH_1_CMD     = 0x2130,
    MEDIUM_1_CMD   = 0x2126,
    LOW_1_CMD      = 0x212D,
    HIGH_2_CMD     = 0x2236,
    MEDIUM_2_CMD   = 0x2220,
    LOW_2_CMD      = 0x222B,
    HIGH_4_CMD     = 0x2334,
    MEDIUM_4_CMD   = 0x2322,
    LOW_4_CMD      = 0x2329,
    HIGH_10_CMD    = 0x2737,
    MEDIUM_10_CMD  = 0x2721,
    LOW_10_CMD     = 0x272A,

	/* Readout of Measurement Results for Periodic Mode */
	READOUT_FOR_PERIODIC_MODE = 0xE000,
} SHT30_CMD;


extern int SHT30_SampleData(float *temperature, float *humidity);


#endif /* INC_SHT30_H_ */
