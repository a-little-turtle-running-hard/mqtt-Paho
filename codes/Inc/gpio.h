/**
  ******************************************************************************
  * @file    gpio.h
  * @brief   This file contains all the function prototypes for
  *          the gpio.c file
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __GPIO_H__
#define __GPIO_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

void MX_GPIO_Init(void);

/* USER CODE BEGIN Prototypes */

/* LED lights*/
enum
{
	SysLed,

	RedLed,

	GreenLed,

	BlueLed,

	LedMax,
};

/* relays */
enum
{
	Relay1,
	Relay2,
	RelayMax,
};

#define	OFF			0
#define	ON			1

typedef struct gpio_s
{
	const char		*name;
	GPIO_TypeDef	*group;
	uint16_t		 pin;

} gpio_t;

extern gpio_t		relays[RelayMax];
extern gpio_t		leds[LedMax];

/* 控制继电器开关函�?*/
extern void turn_relay(int which, int status);

/* 控制LED亮灭的功能函�?*/
void turn_led(int which, int status);

/* 控制LED闪烁函数  */
void blink_led(int which, uint32_t interval);

/* 控制系统指示函数 */
 void sysled_heardbeat(void);

 /* 成功指示�? */
 void success_beat(void);

 /* 失败指示�? */
 void fail_beat(void);

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ GPIO_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
