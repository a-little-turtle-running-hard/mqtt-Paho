/**********************************************************************
 * esp8266.h
 *Copyright:(C)ZhouYanlin<www.zhouyanlin1222@qq.com>
 *
 *Description: for ESP8266-01s to community
 *
 *
 *  Created on: 2021年6月5日
 *      Author: ZhouYanlin
 **********************************************************************/

#ifndef INC_ESP8266_H_
#define INC_ESP8266_H_

/* ESP8266 WiFi模块初始化函数。返回值为0表示成功，!0 表示失败  */
extern int esp8266_module_init(void);


/* ESP8266 WiFi模块复位重启函数。返回值为0表示成功，!0 表示失败  */
extern int esp8266_module_reset(void);


/* ESP8266 WiFi模块连接路由器函数。返回值为0表示成功，!0 表示失败  */
extern int esp8266_join_network(char *ssid, char *pwd);


/* ESP8266 获取自己的IP地址和网关IP地址。返回值为0表示成功，!0 表示失败  */
int esp8266_get_ipaddr(char *ipaddr, char *gateway, int ipaddr_size);


/* ESP8266 WiFi模块做ping命令测试网络连通性。返回值为0表示成功，!0 表示失败  */
int esp8266_ping_test(char *host);


/* ESP8266 WiFi模块建立TCP socket 连接函数。返回值为0表示成功，!0 表示失败 */
extern int esp8266_sock_connect(char *servip, int port);

/* ESP8266 WiFi模块断开TCP socket 连接函数。返回值为0表示成功，!0 表示失败 */
extern int esp8266_sock_disconnect(void);


/* ESP8266 WiFi通过TCP Socket发送数据函数。返回值为0表示失败，>0 表示成功发送字节数 */
extern int esp8266_sock_send(unsigned char *data, int bytes);


/* ESP8266 WiFi通过TCP Socket接收数据函数。返回值为0无数据，>0 表示接收到数据字节数 */
extern int esp8266_sock_recv(unsigned char *buf, int size);


#endif /* INC_ESP8266_H_ */
