/**********************************************************************
 * bc28.c
 *Copyright:(C)ZhouYanlin<www.zhouyanlin1222@qq.com>
 *
 *Description: for NB-IoT to community
 *
 *
 *  Created on: 2021年6月3日
 *      Author: ZhouYanlin
 **********************************************************************/

#include <bc28.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gpio.h"

/* NB_IoT 模块驱动调试宏,注释下面两个宏定义可以取消调试打印 */
//#define CONFIG_NB_IoT_DEBUG
//#define CONFIG_NB_IoT_PRINT

#ifdef CONFIG_NB_IoT_DEBUG
#define nb_dbg(format,args...) printf(format, ##args)
#else
#define nb_dbg(format,args...) do{} while(0)
#endif

#ifdef CONFIG_NB_IoT_PRINT
#define nb_printf(format,args...) printf(format, ##args)
#else
#define nb_printf(format,args...) do{} while(0)
#endif
#define	ON				1
#define	OFF				0

/* 发送温湿度AT指令的前缀字符串 */
#define SEND_TEM_STR	"AT+QLWULDATAEX=9,02000A0004"
#define SEND_HUM_STR	"AT+QLWULDATAEX=9,0200090004"
#define SEND_SIGN_STR	"AT+QLWULDATAEX=9,0200200004"
#define SEND_LUX_STR	"AT+QLWULDATAEX=9,0200150004"

/* 继电器开关状态 */
uint8_t	relay_hum_status = 0;
uint8_t	relay_tem_status = 0;


int nb_iot_reset(void)
{
	nb_printf("NB-IoT start to reset ...\r\n");
	if(  send_atcmd_check_ok("AT+NRB\r\n", 6000) )
	{
		nb_printf("NB-IoT reset failure\r\n");
		return -1;
	}
	nb_printf("NB-IoT reset successfully\r\n");
	return 0;

}

int nb_iot_module_init(char *frequency_band_at)
{
	int       i;

	/* check function input arguments validation */
	if(  !frequency_band_at || strlen(frequency_band_at)<=0 )
	{
		printf("ERROR: Invalid input arguments\r\n");
		return -1;
	}

	nb_printf("INFO: Init NB-IoT module now...\r\n");

	/* 上电到NB模块可以正常接收AT指令约需5秒 此处循环等待 6秒 */
	for(i=0; i<6; i++)
	{
		if( !send_atcmd_check_module() )	/* 发送AT指令检查模块是否正常上电 */
		{
			nb_printf("INFO: Send AT to NB-IoT and got reply ok\r\n");
			break;
		}

		HAL_Delay(100);
	}

	if( i>= 6 )
	{
		nb_printf("ERROR: Can't receive AT replay after reset\r\n");
		return -2;
	}

	/* 重启NB-IoT模块 */
	 if( nb_iot_reset() )
	 {
		 nb_printf("ERROR: Can't receive AT replay after reset\r\n");
		 return-3;
	 }

	/* 关闭射频为修改频段做准备 */
	if( send_atcmd_check_ok("AT+CFUN=0\r\n", 500) )
	{
		nb_printf("ERROR: Can't receive AT replay after reset\r\n");
		return -4;
	}

	/* 修改频段为电信频段 5,8 */
	if( send_atcmd_check_ok(frequency_band_at, 500) )
	{
		nb_printf("ERROR: Can't receive AT replay after reset\r\n");
		return -5;

	}

	/* 打开射频 */
	if( send_atcmd_check_ok("AT+CFUN=1\r\n", 3000) )
	{
		nb_printf("ERROR: Can't receive AT replay after reset\r\n");
		return -6;
	}

	/* 检查SIM卡是否读卡成功 */
	if( send_atcmd_check_ok("AT+CIMI\r\n", 500) )
	{
		nb_printf("ERROR: Can't receive AT replay after reset\r\n");
		return -7;
	}

	return 0;
}


int nb_iot_connect(char *paltform_at)
{
	/* check function input arguments validation */
	if(  NULL == paltform_at || strlen(paltform_at)<=0 )
	{
		printf("ERROR: Invalid input arguments\r\n");
		return -1;
	}

	/* 设置云平台的 IP 和 port */
	if( send_atcmd_check_ok(paltform_at, 500) )
	{
		nb_printf("ERROR: Send CMD:%s get reply failure!\r\n", paltform_at);
		return -2;
	}

	/* 查询网络附着状态，未开则设置模块开始附着网络 */
	if( send_atcmd("AT+CGATT?\r\n", "CGATT:1", 500, NULL, 0) )
	{
		nb_printf("INFO: Net is not sticking and reconnect now\r\n");

		if( send_atcmd("AT+CGATT=1\r\n", "3", 5000, NULL, 0) )
		{
			nb_printf("ERROR: Send sticking net CMD:AT+CGATT=1 get reply failure!\r\n");
			return -3;
		}

	}

	/* 查询平台注册状态，未注册则设置模块开始注册 */
	if( send_atcmd("AT+NMSTATUS?\r\n", "ENABLED", 500, NULL, 0) )
	{
		nb_printf("INFO: Platform is not register and register now!\r\n");

		if( send_atcmd_check_ok("AT+QLWSREGIND=0\r\n", 500) )
		{
			nb_printf("ERROR: Send register CMD: AT+QLWSREGIND=0 get reply failure!\r\n");
			return -4;
		}

	}

	return 0;
}

int nb_iot_get_sign(void)
{
	char *result = NULL;
	char sign_char[2];
	int	 sign_num = -1;
	if( send_atcmd("AT+CSQ\r\n", 0, 500, NULL, 0) )
	{
		nb_printf("ERROR: Send get signal CMD: AT+QLWSREGIND=0 get reply failure!\r\n");
		return -1;
	}

	if( ( result = (strstr(g_atcmd_rxbuf, ":"))) !=NULL )
	{
		sign_char[0] = *(++result);
		sign_char[1] = *(++result);
		sign_num = atoi(sign_char);

		nb_printf("signal strength : %d\r\n", sign_num);
		return sign_num;
	}

}

int nb_iot_send_TH(float temperature, float humidity)
{

	/*  change data to integer from float*/
	int temp = temperature;
	int hum = humidity;
	char tem_buf[64];	/* 存储需要发送的数据格式化好的字符串 */
	char hum_buf[64];

	memset(tem_buf, 0, sizeof(tem_buf) );
	memset(hum_buf, 0, sizeof(hum_buf) );

	snprintf(tem_buf, sizeof(tem_buf),"%s%08x,0x0100\r\n",SEND_TEM_STR,temp);
	snprintf(hum_buf, sizeof(hum_buf),"%s%08x,0x0100\r\n",SEND_HUM_STR,hum);

	if( send_atcmd(tem_buf, "QLWULDATASTATUS:4", 1000, NULL, 0) != 0 )
	{
		return -2;
	}

	HAL_Delay(500);

	if( send_atcmd(hum_buf, "QLWULDATASTATUS:4", 1000, NULL, 0) != 0 )
	{
		return -3;
	}

	nb_printf("temperature:%d	humidity:%d\r\n", temp,hum);

	return 0;
}

int nb_iot_send_sign(int sign)
{
	char sign_buf[64];	/* 存储需要发送的数据格式化好的字符串 */
	memset(sign_buf, 0, sizeof(sign_buf) );

	snprintf(sign_buf, sizeof(sign_buf),"%s%08x,0x0100\r\n",SEND_SIGN_STR,sign);

	if( send_atcmd(sign_buf, "QLWULDATASTATUS:4", 1000, NULL, 0) != 0 )
	{
		return -1;
	}

	return 0;

}

int nb_iot_send_lux(int Lux)
{
	char lux_buf[64];	/* 存储需要发送的数据格式化好的字符串 */
	memset(lux_buf, 0, sizeof(lux_buf) );

	snprintf(lux_buf, sizeof(lux_buf),"%s%08x,0x0100\r\n",SEND_LUX_STR,Lux);
	if( send_atcmd(lux_buf, "QLWULDATASTATUS:4", 1000, NULL, 0)  )
	{
		return -1;
	}

	return 0;
}

int parse_recv_cmd(char * cmd)
{
	/* check function input arguments validation */
	if( NULL == cmd )
	{
		printf("ERROR: Invalid input arguments\r\n");
		return -1;
	}

	if (g_atcmd_bytes > 0)
		{
				HAL_Delay(200);  /* Wait AT command reply receive over */
//				HAL_UART_Transmit(&huart1 , (uint8_t *)g_uart3_rxbuf, g_uart3_bytes, 0xFF);

				if(strstr(cmd, "8,061F45") != NULL)	//控制温度继电器指令
				{
					HAL_Delay(1);

					if( (strstr(cmd, "0101") != NULL) && (OFF == relay_tem_status) )	//打开继电器
					{
						turn_relay(Relay2, ON);
						turn_led(RedLed, ON);
						printf("Turn temperature relay ON!!!\r\n");
						printf("-----------------\r\n");
						relay_tem_status = ON;
					}

					/* 关闭继电器 */
					else if(ON == relay_tem_status)
					{
						turn_relay(Relay2, OFF);
						turn_led(RedLed, OFF);
						printf("Turn temperature relay OFF!!!\r\n");
						printf("-----------------\r\n");
						relay_tem_status = OFF;
					}

				}

				else if( (strstr(cmd, "8,061F47") != NULL) )	//控制湿度继电器指令
				{
					HAL_Delay(1);

					if( (strstr(cmd, "0101") != NULL) && (OFF == relay_hum_status) )	//打开继电器
					{
						turn_relay(Relay1, ON);
						turn_led(BlueLed, ON);
						printf("Turn humidity relay ON!!!\r\n");
						printf("-----------------\r\n");
						relay_hum_status = ON;
					}

					else if(ON == relay_hum_status)
					{
						turn_relay(Relay1, OFF);
						turn_led(BlueLed, OFF);
						printf("Turn humidity relay OFF!!!\r\n");
						printf("-----------------\r\n");
						relay_hum_status = OFF;
					}

				}

//				clear_atcmd_rxbuf();
				return 0;
		}

}
