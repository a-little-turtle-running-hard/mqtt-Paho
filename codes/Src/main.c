/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <string.h>
#include "sht30.h"
#include "core_json.h"
#include "bc28.h"
#include "esp8266.h"
#include "core_mqtt.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

#define  DBG_PRINT
#ifdef 	 DBG_PRINT
#define dbg_printf(format,args...) printf(format, ##args)
#else
#define dbg_printf(format,args...) do{} while(0)
#endif

#define	SEND_INTERVAL	 30000

#define DEF_ROUTER_SSID          "616靓仔"
#define DEF_ROUTER_PWD           "KK201908364120"

#define MQTT_BROKER_HOSTNAME     "weike-iot.com"  //设置连接MQTT Broker服务器IP
#define MQTT_BROKER_PORT         1883    		  //设置MQTT Broker监听的端口号
#define MQTT_BROKER_USERNAME     "lingyun"
#define MQTT_BROKER_PASSWORD     "lingyun"

#define MQTT_CLIENT_ID           "BearKE-8266"  	//设置MQTT本设备ID
#define MQTT_PUB_TOPIC           "$Sys/Studio/Uplink/"MQTT_CLIENT_ID
#define MQTT_SUB_TOPIC           "$Sys/Studio/Downlink/"MQTT_CLIENT_ID

#define FLAG_WIFI_CONNECTED     (1<<0)	/* 1 */
#define FLAG_SOCK_CONNECTED     (1<<1)	/*10 */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/*
 * 函数说明：
 * 以json的形式上报温湿度
 *
 * 返回值：
 * >0:表示发送的字节数; -1：表示采样失败 ；
 * -2：表示发送失败
 *
 * */
int report_temp_and_humi_json(void);


/*
 * 函数说明：
 * 解析传入的JSON报文做出相应的控制
 *
 * 返回值：
 * 1:表示解析并执行命令成功; -1：表示收到的数据非JSON格式；
 *
 * */
int parser_led_json(char *json_string, int bytes);

/*
 * 函数说明：
 * 订阅MQTT主题
 *
 * */
void mqtt_subscribe_proc(void);


/*
 * 函数说明：
 * 处理接AT串口收到的数据并做出相应控制
 *
 * */
void proc_atcmd_recv(void);

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

	unsigned char       wifi_flag = 0;
	char                ipaddr[16];
	char                gateway[16];
	uint32_t			latest_time = 0;
	int					rv = -1;

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_USART3_UART_Init();
  MX_TIM6_Init();
  MX_ADC1_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  printf("The program start to run ...\r\n");
  sysled_heardbeat();

  esp8266_module_init();

  while (1)
  {
//	  uart_forward();	/* 串口转发调试�? */

	  if( ! (wifi_flag&FLAG_WIFI_CONNECTED) )
	  {
		  if( esp8266_join_network(DEF_ROUTER_SSID, DEF_ROUTER_PWD) )
		  {
			  esp8266_module_init();
			  HAL_Delay(3000);
			  continue;
		  }

		  if( esp8266_get_ipaddr(ipaddr, gateway, sizeof(ipaddr) ) )
		  {
			  HAL_Delay(1000);
			  continue;
		  }

		  if( esp8266_ping_test(gateway) )
		  {
			  HAL_Delay(1000);
			  continue;
		  }

		  wifi_flag |= FLAG_WIFI_CONNECTED; /* set wifi connected flag */
	  }

	  if( ! (wifi_flag&FLAG_SOCK_CONNECTED) )
	  {
		  	rv = mqtt_connect(MQTT_BROKER_HOSTNAME, MQTT_BROKER_PORT, MQTT_CLIENT_ID,
			MQTT_BROKER_USERNAME, MQTT_BROKER_PASSWORD);

		  if( rv )
		  {
			  HAL_Delay(1000);
			  continue;
		  }

          /* 连上MQTT Broker后订阅下发三色Led控制命令的主题 */
		  mqtt_subscribe_topic(MQTT_SUB_TOPIC, Qos0, 1);

		  wifi_flag |= FLAG_SOCK_CONNECTED; /* set socket connected flag */
	  }

      /* 处理MQTT 订阅收到的消息 */
      mqtt_subscribe_proc();


	  /* 使用 time_afert 宏计�? 解决时间回绕和调用delay延时浪费CUP */
	  if(time_after(HAL_GetTick(),latest_time+SEND_INTERVAL))
	  {

		  /* Get temperature and humidity */
		  rv = report_temp_and_humi_json();
		  if ( 1== rv)
		  {
			  printf("Sample data failure and retry now!\r\n");
			  HAL_Delay(1000);
			  continue;
		  }
		  else if(rv < -1)
		  {
			  printf("Send data failure and will reset...\r\n");
			  wifi_flag &= ~FLAG_SOCK_CONNECTED; /* clear socket connected flag */

			  if( esp8266_ping_test(gateway) )
			  {
				  wifi_flag &= ~FLAG_WIFI_CONNECTED; /* clear wifi connected flag */

			  }
		  }

		  latest_time = HAL_GetTick();

	  }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 20;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_ADC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 9;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV6;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

int report_temp_and_humi_json(void)
{
	int			  rv = -1;
	char          buf[128];
    float         temperature, humidity;

    if ( SHT30_SampleData(&temperature, &humidity) < 0 )
    {
    	printf("ERROR: SHT30 Sample data failure\n");
    	return 1;
    }

    memset(buf, 0, sizeof(buf));
    snprintf(buf, sizeof(buf), "{\"Temperature\":\"%.2f\", \"Humidity\":\"%.2f\"}\r\n", temperature, humidity);
    dbg_printf("buffer:%s",buf);
    printf("Sample data successfully!\r\nTemperature%.2f Humidity:%.2f", temperature, humidity);

    rv = mqtt_publish(MQTT_PUB_TOPIC, Qos0, buf);
    printf("MQTT publish topic [%s] with msg '%s' %s, rv=%d\r\n", MQTT_PUB_TOPIC, buf, rv ? "failure": "ok", rv);
    return rv;

}

int parser_led_json(char *json_string, int bytes)
{
   JSONStatus_t             result;
    char                     save;
    char                    *value;
    size_t                   valen;
    int                      i;

    printf("DBUG: Start parser JSON string: %s\r\n", json_string);

	result = JSON_Validate(json_string, bytes);

	/* JSON document is valid so far but incomplete */
	if( JSONPartial == result )
	{
		printf("WARN: JSON document is valid so far but incomplete!\r\n");
		return 0;
	}

	/* JSON document is not valid JSON */
	if( JSONSuccess != result )
	{
		printf("ERROR: JSON document is not valid JSON!\r\n");
		return -1;
	}

	/* Parser and set LED status */
	for(i=0; i<LedMax; i++)
	{
		result = JSON_Search( json_string, bytes,  leds[i].name, strlen(leds[i].name), &value, &valen);
		if( JSONSuccess == result )
		{
			 save = value[valen];
			 value[valen] = '\0';

			 if( !strncasecmp(value, "on", 2) )
			 {
				 printf("DBUG: turn %s on\r\n", leds[i].name);
				 turn_led(i, ON);
			 }
			 else if( !strncasecmp(value, "off", 3) )
			 {
				 printf("DBUG: turn %s off\r\n", leds[i].name);
				 turn_led(i, OFF);
			 }

			 value[valen] = save;
		}
	}

	return 1;
}

void proc_atcmd_recv(void)
{
	  if( g_atcmd_bytes > 0 )
	  {
		     HAL_Delay(200);
		     if( 0 != parser_led_json(g_atcmd_rxbuf, g_atcmd_bytes) )
		     {
		    	 clear_atcmd_rxbuf();
		     }
	  }
}

void mqtt_subscribe_proc(void)
{
    unsigned char                      buf[256];;
    int                                packet_type;
    MQTTString                         topicName;
    unsigned char                      dup;
    int                                qos;
    unsigned char                      retained;
    unsigned short                     msgid;
    unsigned char                      *payload = NULL;
    int                                payloadlen = 0;
    int                                rv;

    memset(buf, 0, sizeof(buf));

    packet_type = MQTTPacket_read(buf, sizeof(buf), transport_getdata);
    if( packet_type < 0 )
    {
    	return ;
    }

    switch( packet_type )
    {
    	case 0:
    		break;

		case PUBLISH:
		{
			rv = MQTTDeserialize_publish(&dup, &qos, &retained, &msgid, &topicName, &payload, &payloadlen, buf, sizeof(buf));
			if ( rv == 1)
			{
				printf("MQTT Payload: %s\n", payload);
				parser_led_json((char *)payload, payloadlen);
			}

			else
			{
				printf("ERROR: MQTTDeserialize_publish() failure, rv=%d\r\n", rv);
			}
			break;
		}

		case CONNACK:
		case PUBACK:
		case SUBACK:
		case UNSUBACK:
		case PUBREC:
		case PUBREL:
		case PUBCOMP:
			break;
    }
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
