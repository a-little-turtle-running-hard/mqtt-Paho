/**********************************************************************
 * transport.h
 *Copyright:(C)ZhouYanlin<www.zhouyanlin1222@qq.com>
 *
 *Description: Paho MQTT library protable lowlevel API for ESP8266
 *
 *
 *  Created on: 2021年10月21日
 *      Author: ZhouYanlin
 **********************************************************************/

#ifndef SRC_MQTT_TRANSPORT_H_
#define SRC_MQTT_TRANSPORT_H_

/* 使用ESP8266创建socket 连接到 MQTT服务器函数 */
extern int transport_open(char* host, int port);

/* ESP8266关闭socket 连接函数 */
extern int transport_close(void);

/* 使用ESP8266发送一个MQTT数据报文的函数 */
extern int transport_sendPacketBuffer(unsigned char* buf, int buflen);

/* 使用ESP8266接收MQTT数据报文函数 */
extern int transport_getdata(unsigned char* buf, int count);

/* 清除ESP8266 接收MQTT数据的socket buffer函数 */
extern void transport_clearBuf(void);

#endif /* SRC_MQTT_TRANSPORT_H_ */
