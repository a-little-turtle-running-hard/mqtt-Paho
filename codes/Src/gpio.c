/**
  ******************************************************************************
  * @file    gpio.c
  * @brief   This file provides code for the configuration
  *          of all used GPIO pins.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
*/
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, Relay1_Pin|Relay2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, SysLed_Pin|BlueLed_Pin|RedLed_Pin|GreenLed_Pin, GPIO_PIN_SET);

  /*Configure GPIO pins : PBPin PBPin PBPin PBPin
                           PBPin PBPin */
  GPIO_InitStruct.Pin = Relay1_Pin|Relay2_Pin|SysLed_Pin|BlueLed_Pin
                          |RedLed_Pin|GreenLed_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 2 */

/* LED引脚定义*/
gpio_t	leds[LedMax] =
{
		{ "SysLed",		SysLed_GPIO_Port, 	SysLed_Pin },
		{ "RedLed",		RedLed_GPIO_Port, 	RedLed_Pin },
		{ "GreenLed", 	GreenLed_GPIO_Port, GreenLed_Pin },
		{ "BlueLed",	BlueLed_GPIO_Port,	BlueLed_Pin },

};

/* 继电器引脚定�?*/
gpio_t		relays[RelayMax] =
{
		{"Relay1",	Relay1_GPIO_Port,	Relay1_Pin},
		{"Relay2",	Relay2_GPIO_Port,	Relay2_Pin},

};

void turn_relay(int which, int status)
{
	GPIO_PinState	level;

	if( which >= RelayMax)
	{
		return ;
	}

	level = status == OFF ? GPIO_PIN_RESET : GPIO_PIN_SET;

	HAL_GPIO_WritePin(relays[which].group, relays[which].pin, level);

}

void turn_led(int which, int status)
{
	GPIO_PinState		level;

	if( which >= LedMax )
		return ;

	level = (OFF==status) ? GPIO_PIN_SET : GPIO_PIN_RESET;

	HAL_GPIO_WritePin(leds[which].group, leds[which].pin, level);

}

/* 函数说明: 控制LED闪烁  */
  void blink_led(int which, uint32_t interval)
  {

	  turn_led( which, ON);
	  HAL_Delay(interval);

	  turn_led( which, OFF);
	  HAL_Delay(interval);

  }

  /* 系统指示�? */
 void sysled_heardbeat(void)
  {
	  blink_led(SysLed, 100);
	  blink_led(SysLed, 100);
	  blink_led(SysLed, 800);

  }

 /* 成功指示�? */
void success_beat(void)
   {
 	  blink_led(SysLed, 100);

   }

/* 失败指示�? */
void fail_beat(void)
  {
	  blink_led(SysLed, 500);

  }


/* USER CODE END 2 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
