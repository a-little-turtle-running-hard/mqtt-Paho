# 基于STM32智能家居实时监控系统

## 项目简介
 **项目基于：** STM32 + ESP8266WiFi模块 + SH30温湿度传感器 + MQTT-Paho移植库 + MQTT服务器 + MQTT客户端设计

此系统的主要功能是将终端（SHT30温湿度传感器）采集到的 **温湿度** ，使用ESP8266WiFi模块通过MQTT协议将数据传输至MQTT服务器。用户通过 **MQTT手机客户端** 连接至MQTT服务器并 **订阅** STM32终端发布的 **主题** ，实现对家居的 **实时检测功能** ，通过向STM32终端订阅的主题 **发布** 命令消息，实现对终端的 **实时控制功能** 。

## 主要功能


1. 使用SHT30采集终端环境的温湿度
1. 使用ESP8266WiFi模块实现STM32连接网络
1. 将MQTT-Paho库移植至STM32实现MQTT协议通信
1. 使用ESP8266WiFi模块通过MQTT协议将终端采集的数据发送至MQTT服务器
1. 将ESP8266WiFi模块收到的命令消息进行解析并做出相应控制

## 系统架构图
![输入图片说明](doc/%E7%B3%BB%E7%BB%9F%E6%9E%B6%E6%9E%84%E5%9B%BEimage.png)

## 功能演示

### 使用SHT30采集终端环境的温湿度
![输入图片说明](doc/SHT30%E9%87%87%E9%9B%86%E7%BB%88%E7%AB%AF%E7%8E%AF%E5%A2%83%E6%B8%A9%E6%B9%BF%E5%BA%A6image.png)

### 使用ESP8266WiFi模块实现STM32连接网络
![输入图片说明](doc/%E4%BD%BF%E7%94%A8ESP8266WiFi%E6%A8%A1%E5%9D%97%E5%AE%9E%E7%8E%B0STM32%E8%BF%9E%E6%8E%A5%E7%BD%91%E7%BB%9Cimage.png)

### 使用ESP8266WiFi模块通过MQTT协议将终端采集的数据发送至MQTT服务器
#### 终端成功发送数据
![输入图片说明](doc/%E7%BB%88%E7%AB%AF%E6%88%90%E5%8A%9F%E5%8F%91%E9%80%81%E6%95%B0%E6%8D%AEimage.png)

#### MQTT手机APP订阅端收到消息
![输入图片说明](doc/MQTT%E6%89%8B%E6%9C%BAAPP%E8%AE%A2%E9%98%85%E7%AB%AF%E6%8E%A5%E6%94%B6%E5%88%B0%E6%B6%88%E6%81%AFimage.png)

### 将ESP8266WiFi模块收到的命令消息进行解析并做出相应控制
#### MQTT手机APP向终端订阅的主题发布打开蓝灯JSON命令
![输入图片说明](doc/MQTT%E6%89%8B%E6%9C%BAAPP%E5%90%91%E7%BB%88%E7%AB%AF%E8%AE%A2%E9%98%85%E7%9A%84%E4%B8%BB%E9%A2%98%E5%8F%91%E5%B8%83%E6%89%93%E5%BC%80%E8%93%9D%E7%81%AFJSON%E5%91%BD%E4%BB%A4image.png)

#### 终端接收到命令解析并执行命令内容
![输入图片说明](doc/%E7%BB%88%E7%AB%AF%E6%8E%A5%E6%94%B6%E5%88%B0%E5%91%BD%E4%BB%A4%E8%A7%A3%E6%9E%90%E5%B9%B6%E6%89%A7%E8%A1%8C%E5%91%BD%E4%BB%A4%E5%86%85%E5%AE%B9image.png)





